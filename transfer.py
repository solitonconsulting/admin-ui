# -*- coding: utf-8 -*-
from commands import AwaitZopeReady
from commands import StartZope
from commands import StopZope
from interfaces import CommandMacro
from interfaces import IIntervalState
from interfaces import IObservableCommand
from progress import ProgressBar
from pywebio.output import put_loading
from pywebio.output import put_text

import subprocess


class DownloadNumberOfFiles(ICommand):
    def execute(self):
        with put_loading():
            _ = subprocess.run(
                ['sudo',
                 './download-number-of-blobsnapshots.sh',  # TODO path
                ],
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE
            )
            nbremote, nblocal = _.stdout.decode().split(',')
            put_text(f'Number of remote blobs: {nbremote}')
            put_text(f'Number of local blobs: {nblocal}')


class DownloadSnapshotBackups(IIntervalState, IObservableCommand):
    """
    sudo -u plone_daemon rsync -a --delete -H -S --log-file=/tmp/snapshotbackups rsync://$HOST/colonial_art_backup/snapshotbackups/ /var/local/plone-4.3/zeoserver/snapshotbackups/
    """
    def execute(self):
        self.addObserver(ProgressBar())
        with put_loading():
            put_text('Downloading snapshots')
            _ = subprocess.Popen(
                ['sudo',
                 './download-snapshotbackups.sh',  # TODO path
                ])
            self.watch_log_file()

    def watch_log_file(self):
        self.set_interval(duration=160)
        for _ in self.interval_generator():
            self.notifyObservers()


class DownloadBlobStorageSnapshots(IIntervalState, IObservableCommand):
    """
    sudo -u plone_daemon rsync -a --delete -H -S --log-file=/tmp/blobstoragesnapshots rsync://$HOST/colonial_art_backup/blobstoragesnapshots/ /var/local/plone-4.3/zeoserver/blobstoragesnapshots/
    """
    def execute(self):
        self.addObserver(ProgressBar())
        with put_loading():
            put_text('Downloading blobs')
            _ = subprocess.Popen(
                ['sudo',
                 './download-blobstoragesnapshots.sh',  # TODO path
                ])
            self.watch_log_file()

    def watch_log_file(self):
        self.set_interval(duration=300)
        for _ in self.interval_generator():
            self.notifyObservers()


class RestoreSnapshot(IIntervalState, IObservableCommand):
    """
    sudo -u plone_daemon /usr/local/plone-4.3/zeoserver/bin/snapshotrestore -n -q
    """
    def execute(self):
        self.addObserver(ProgressBar())
        with put_loading():
            put_text('Restoring data from snapshots')
            _ = subprocess.Popen(
                ['sudo',
                 './restore-snapshots.sh',
                ])
            self.watch_datafs_size()

    def watch_datafs_size(self):
        self.set_interval(duration=30)
        for _ in self.interval_generator():
            self.notifyObservers()


restore_from_snapshots = CommandMacro([
    DownloadNumberOfFiles(),
#    DownloadSnapshotBackups(),
#    DownloadBlobStorageSnapshots(),
    StopZope(),
#    RestoreSnapshot(),
    StartZope(),
    AwaitZopeReady(),
])
