# -*- coding: utf-8 -*-
from abc import ABC
from abc import abstractmethod
from datetime import datetime

import time


class IObserver(ABC):

    @abstractmethod
    def update(self, state): raise NotImplementedError


class IObservable(ABC):
    observers = []

    def addObserver(self, observer):
        self.observers.append(observer)

    def removeObserver(self, observer):
        self.observers.remove(observer)

    def notifyObservers(self):
        for ob in self.observers:
            ob.update(self.state)

    @property
    def state(self): raise NotImplementedError


class ICommand(ABC):

    @abstractmethod
    def execute(self): raise NotImplementedError


class ICommandInvoker(ABC):

    commands = {}

    def setCommand(self, key, command):
        self.commands[key] = command


class ICommandReceiver(ABC):
    pass


class IObservableCommand(ICommand, IObservable):
    pass


class CommandMacro(ICommand):
    command = None
    _next_command_ = None

    def __init__(self, command_or_list=None):
        if isinstance(command_or_list, ICommand):
            self.command = command_or_list
        if isinstance(command_or_list, list):
            self.command = command_or_list.pop(0)
            if command_or_list:
                self.next_command = self.__class__(command_or_list)

    @property
    def next_command(self):
        return self._next_command_

    @next_command.setter
    def next_command(self, command_macro=None):
        self._next_command_ = command_macro

    def execute(self):
        self.command.execute()
        if self.next_command:
            self.next_command.execute()


class IIntervalState(ABC):

    def set_interval(self, initial=datetime.now(), duration=15):
        self._initial_ = initial
        self.total_duration = duration

    @property
    def state(self):
        delta = datetime.now() - self._initial_
        return round(delta.total_seconds() / self.total_duration, 2)

    def interval_generator(self, n_intervals=50):
        self._initial_ = datetime.now()
        for i in range(n_intervals):
            time.sleep(self.total_duration / n_intervals)
            yield


class IInterruptibleCommand(ICommand):
    """ # TODO
    """
