#!/bin/bash
set -euo pipefail

source sourcerer.sh
RSYNC_LOG=/tmp/size_of_blobs

set +e
RSYNC_PASSWORD=$RSYNC_PASSWORD rsync -a --delete -H -S --log-file=$RSYNC_LOG -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i $PRIVATE_KEY" rsync://$RSYNC_USER@$RSYNC_HOST/colonial_art_data/size_of_blobs $LOGPATH/size_of_blobs.remote
RESULT=$?
set -e

if [ $RESULT -ne 0 ]; then
    echo 'Unsuccessful rsync attempt for size_of_blobs' > $LOGFAILURE
    if [ -f $RSYNC_LOG ]; then
        cat $RSYNC_LOG >> $LOGFAILURE
    fi
    exit 1
fi

AFTER=$(date +%s)
DURATION=$(($AFTER - $BEFORE))

echo $DURATION seconds > $LOGSUCCESS
LINES=$(wc -l $RSYNC_LOG)
echo $LINES lines >> $LOGSUCCESS
echo $RSYNC_LOG >> $LOGSUCCESS
rm $RSYNC_LOG

echo $(cat $LOGPATH/size_of_blobs.remote),$(cat $STORAGE_PATH/size_of_blobs)
