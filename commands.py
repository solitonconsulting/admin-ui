# -*- coding: utf-8 -*-
from interfaces import CommandMacro
from interfaces import IIntervalState
from interfaces import ICommand
from interfaces import IObservableCommand
from progress import ProgressBar
from pywebio.output import put_loading
from pywebio.output import put_text

import subprocess


class StopZope(ICommand):
    def execute(self):
        with put_loading():
            put_text('Stopping Zope')
            _ = subprocess.run(
                ['sudo',
                 'supervisorctl',
                 'stop',
                 'all',
                 ],
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                )
            put_text(_.stdout.decode())


class StartZope(ICommand):
    def execute(self):
        with put_loading():
            put_text('Starting Zope')
            _ = subprocess.run(
                ['sudo',
                 'supervisorctl',
                 'start',
                 'all',
                 ],
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                )
            put_text(_.stdout.decode())


class AwaitZopeReady(IIntervalState, IObservableCommand):
    def execute(self):
        """ Simplistic approach:  just wait for n seconds.
        Possible approaches:
        1) read the Zope event log and calculate the startup interval
        of the last time the app was started.
        2) watch the Zope event log and count the number of lines
        appended since startup, until it reaches the expected number.
        """
        self.addObserver(ProgressBar())
        self.set_interval()
        put_text('Waiting for client to be ready')
        for _ in self.interval_generator(50):
            self.notifyObservers()


StartLocalSiteCommand = CommandMacro([
    StopZope(),
    StartZope(),
    AwaitZopeReady(),
])
