#!/bin/bash

#   This script is meant to be run as root as it needs access to supervisor
#   and to start nginx

if [ `whoami` != 'root' ]; then
    echo "The restart script needs to control nginx and supervisor."
    echo "It must be run as superuser -- typically via sudo."
    exit 1
fi

FLUSH="yes"
for option; do
    case $option in
        --noflush | noflush | -noflush | --NOFLUSH | NOFLUSH | -NOFLUSH)
            FLUSH="no"
        ;;
        *)
            echo "usage: restart_clients.sh [noflush]"
            echo
            echo "Specify 'noflush' to skip flushing the varnish cache."
            exit 1
        ;;
    esac
done



echo Restarting client 1
supervisorctl stop zeoserver_zeoclient1
supervisorctl start zeoserver_zeoclient1
echo Restarting client 2
supervisorctl stop zeoserver_zeoclient2
supervisorctl start zeoserver_zeoclient2

echo Done
