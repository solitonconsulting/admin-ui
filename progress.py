# -*- coding: utf-8 -*-
from interfaces import IObserver
from pywebio.output import put_processbar
from pywebio.output import set_processbar


class ProgressBar(IObserver):
    name = None

    def update(self, state):
        if not self.name:
            self.name = self.__hash__()
            put_processbar(self.name, auto_close=False)
        else:
            set_processbar(self.name, state)
