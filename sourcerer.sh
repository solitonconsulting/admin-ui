#!/bin/bash
set -euo pipefail

set +e
MACHINE_ID=$(cat /etc/machine-id || "none")
set -e
NOW=$(date +%Y%m%d%H%M%S)
BEFORE=$(date +%s)
SCRIPTNAME=$(basename -- $0)
LOGNAME=${SCRIPTNAME%.*}
LOGPATH=/var/log/virtualbox-admin
LOGFILE=$LOGPATH/$LOGNAME-$NOW-$MACHINE_ID
LOGSUCCESS=$LOGFILE-success.log
LOGFAILURE=$LOGFILE-failure.log

# The file .env is expected to define the following variables:
# REPOPATH
# RSYNC_PASSWORD
# RSYNC_USER
# RSYNC_GROUP
# RSYNC_HOST
# PRIVATE_KEY
# ANSIBLE_PRIVATE_KEY
# SNAPSHOT_PATH
# STORAGE_PATH

if ! source .env; then
  echo '.env file is missing' > $LOGFAILURE
  exit 1
fi
