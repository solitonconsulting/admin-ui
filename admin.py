# -*- coding: utf-8 -*-
from commands import StartLocalSiteCommand
from pywebio import session
from pywebio.input import * # noqa
from pywebio.input import radio
from pywebio.output import * # noqa
from pywebio.output import put_markdown
from pywebio.output import put_text
from pywebio.output import toast
from transfer import restore_from_snapshots

import os
import signal
import time


class Launcher:
    """ Offline Site Launcher

    Select how to start your offline site
    """
    def __init__(self):
        pass

    def __call__(self):
        self.menu()

        # command = MenuFactory(onlinestate).

    def menu(self):

        put_text('Welcome to your offline Plone site').show()
        put_markdown('## Welcome to offline Plone!').show()
        put_markdown('# Plone').show()
        put_text('Your offline site is ready to start.')
        put_text('Your offline site is ready to start.').show()
        answer = radio('Your offline data was last updated Sep 2, 2021 3:02:54 pm PST', options=[
          'Resume site from previous session',
          'Download changes from your remote site since last update before starting site',
          'Clear offline data and download fresh copy from your remote site',
          ]
        )

        # StartLocalSiteCommand.execute()
        restore_from_snapshots.execute()

        put_markdown('## Welcome to your offline Plone site!').show()
        # #put_code(put_scope('scrollable'), rows=10)
        # with use_scope('scrollable'):
        #     put_text("You can click the area to prevent auto scroll.", scope='scrollable')

        #     start_time = time.time()
        #     while start_time + 3 > time.time():
        #         put_code(time.time(), scope='scrollable')
        #         time.sleep(0.5)

        session.run_js('console.log("foo"); WebIO._state.CurrentSession.on_session_close(()=>{setTimeout(()=>location.reload(), 500)})')
        with put_loading():
            toast("Your site will load in a second.🔔")
            time.sleep(1)
        os.kill(os.getpid(), signal.SIGTERM)
