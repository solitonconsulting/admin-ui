#!/bin/bash
set -euo pipefail

source sourcerer.sh
echo $LOGFILE

cd $REPOPATH

if ! (git fetch) then
  echo 'Unsucceful fetch attempt from remote' > $LOGFAILURE
  exit 1
fi

if ! (git merge) then
  echo 'Unsuccessful merge attempt' > $LOGFAILURE
  exit 1
fi

echo 'Successfully pulled from remote' > $LOGSUCCESS


RSYNC_PASSWORD=$RSYNC_PASSWORD rsync -rltz -H -S -e  "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i $PRIVATE_KEY" $LOGPATH/* rsync://$RSYNC_USER@$RSYNC_HOST/logfile_inbox/

