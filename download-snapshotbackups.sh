#!/bin/bash
set -euo pipefail

source sourcerer.sh
RSYNC_LOG=/tmp/snapshotbackups

set +e
RSYNC_PASSWORD=$RSYNC_PASSWORD rsync -a --delete -H -S --log-file=$RSYNC_LOG -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i $PRIVATE_KEY" rsync://$RSYNC_USER@$RSYNC_HOST/colonial_art_backup/snapshotbackups/ /var/local/plone-4.3/zeoserver/snapshotbackups/
RESULT=$?
set -e

if [ $RESULT -ne 0 ]; then
    echo 'Unsuccessful rsync attempt for snapshotbackups' > $LOGFAILURE
    if [ -f $RSYNC_LOG ]; then
        cat $RSYNC_LOG >> $LOGFAILURE
    fi
    exit 1
fi

AFTER=$(date +%s)
DURATION=$(($AFTER - $BEFORE))

echo $DURATION seconds > $LOGSUCCESS
LINES=$(wc -l $RSYNC_LOG)
echo $LINES lines >> $LOGSUCCESS
echo $(tail -1 $RSYNC_LOG) >> $LOGSUCCESS
rm $RSYNC_LOG
