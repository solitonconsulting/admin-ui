# -*- coding: utf-8 -*-
from commands import AwaitZopeReady
from commands import StartZope
from commands import StopZope
from interfaces import CommandMacro
from interfaces import IIntervalState
from interfaces import IObservableCommand
from progress import ProgressBar
from pywebio.output import put_loading
from pywebio.output import put_text

import subprocess


# download sizes
# download tarball
# rename blobstorage
# rename Data.fs
# StopZope()
# extract tarball
# StartZope()
# delete saved blobstorage and Data.fs
# AwaitZopeReady()

class DownloadSizes(ICommand):
    def execute(self):
        with put_loading():
            _ = subprocess.run(
                ['sudo',
                 './download-size-of-datafs.sh',  # TODO path
                ],
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE
            )
            nbremote, nblocal = _.stdout.decode().split(',')
            put_text(f'Number of remote blobs: {nbremote}')
            put_text(f'Number of local blobs: {nblocal}')
            _ = subprocess.run(
                ['sudo',
                 './download-number-of-blobsnapshots.sh',  # TODO path
                ],
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE
            )
            nbremote, nblocal = _.stdout.decode().split(',')
            put_text(f'Number of remote blobs: {nbremote}')
            put_text(f'Number of local blobs: {nblocal}')
