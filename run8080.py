# -*- coding: utf-8 -*-
from admin import Launcher

import pywebio


if __name__ == '__main__':
    pywebio.start_server(Launcher(), port=8080, debug=True)
