>>> from pywebio.input import *
>>> from pywebio.output import *
>>> put_text('Welcome to colonialart').show()
>>> put_markdown('## Welcome to colonialart!').show()
>>> put_markdown('# PESSCA').show()
>>> put_text('Your offline site is ready to start.')
<pywebio.io_ctrl.Output object at 0x7a12c16fd3c8>
>>> put_text('Your offline site is ready to start.').show()
>>> clear()
>>> answer = radio('Choose one', options=[
...   'Resume site from previous session',
...   'Update data from colonialart.org before starting site',
...   'Clear offline data and download fresh copy from colonialart.org',
...   ]
... ).show()
Traceback (most recent call last):
  File "<stdin>", line 4, in <module>
AttributeError: 'str' object has no attribute 'show'
>>> answer = radio('Your offline data was last updated Sep 2, 2021 3:02:54 pm PST', options=[
...   'Resume site from previous session',
...   'Download changes from colonialart.org since last update before starting site',
...   'Clear offline data and download fresh copy from colonialart.org',
...   ]
... )
