#!/bin/bash
set -euo pipefail

source sourcerer.sh

echo $LOGSUCCESS

set +e
/usr/local/plone-4.3/zeoserver/bin/snapshotrestore -n -q >& /tmp/$LOGNAME
RESULT=$?
set -e

if [ $RESULT -ne 0 ]; then
    echo 'Unsuccessful snapshotrestore attempt ' > $LOGFAILURE
    cat /tmp/$LOGNAME >> $LOGFAILURE
    rm /tmp/$LOGNAME
    exit 1
fi

AFTER=$(date +%s)
DURATION=$(($AFTER - $BEFORE))

echo $DURATION > $LOGSUCCESS
